package nl.hanze.student;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "student", propOrder =
	{ "naam", "leeftijd", "geslacht" })
@XmlRootElement(name = "student")
public class Student
{
	@XmlElement(required = true)
	private String naam;
	@XmlElement(required = true)
	private int leeftijd;
	@XmlElement(required = true)
	private boolean geslacht;

	public String getNaam()
	{
		return naam;
	}

	public void setNaam(String naam)
	{
		this.naam = naam;
	}

	public int getLeeftijd()
	{
		return leeftijd;
	}

	public void setLeeftijd(int leeftijd)
	{
		this.leeftijd = leeftijd;
	}

	public boolean isGeslacht()
	{
		return geslacht;
	}

	public void setGeslacht(boolean geslacht)
	{
		this.geslacht = geslacht;
	}

	/**
	 * Return this object as an XML string.
	 * 
	 * @return
	 */
	public String toXML()
	{
		try
		{
			JAXBContext context = JAXBContext.newInstance(getClass());
			StringWriter writer = new StringWriter();
			context.createMarshaller().marshal(this, writer);
			return writer.toString();
		}
		catch (JAXBException e)
		{
			// This usually doesn't happen.
			throw new RuntimeException(e);
		}
	}
}