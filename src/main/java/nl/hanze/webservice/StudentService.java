package nl.hanze.webservice;

import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 * Student webservice.
 * 
 * @author Johan Mulder
 */
@WebService(serviceName = "StudentService")
public class StudentService
{
	/**
	 * Student service method.
	 * 
	 * @return
	 */
	@WebMethod
	public nl.hanze.student.Student getStudent()
	{
		nl.hanze.student.Student student = new nl.hanze.student.Student();
		student.setGeslacht(true);
		student.setLeeftijd(35);
		student.setNaam("Test");
		return student;
	}
}
