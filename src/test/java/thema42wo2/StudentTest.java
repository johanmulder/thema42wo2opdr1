package thema42wo2;

import static org.junit.Assert.*;
import nl.hanze.student.Student;

import org.junit.Test;

public class StudentTest
{

	@Test
	public void testToXml()
	{
		Student student = new Student();
		student.setGeslacht(true);
		student.setLeeftijd(35);
		student.setNaam("Test");
		String xml = student.toXML();
		assertNotNull(xml);
		// At least this part is required in the xml. Ignore the XML header.
		assertTrue(xml.endsWith("<student><naam>Test</naam><leeftijd>35</leeftijd><geslacht>true</geslacht></student>"));
	}

}
